package api;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pageObjects.AbstractPage;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ApiPage extends AbstractPage {
    private final  By tagSelector = By.tagName("a");

    public ArrayList<String> checkLinksOnThePage() throws IOException {
        int notFoundStatusCode = 404;
        String url = "";
        int respCode;
        HttpURLConnection huc;
        List<WebElement> links = getElements(tagSelector);
        ArrayList<String> badRequestLinksList = new ArrayList<>();
        try {
            for (WebElement link : links) {
                url = link.getAttribute("href");
                if (url != null) {
                    huc = (HttpURLConnection) (new URL(null, url, new sun.net.www.protocol.https.Handler()).openConnection());
                    huc.setRequestMethod("HEAD");
                    huc.connect();
                    respCode = huc.getResponseCode();
                    if (respCode == notFoundStatusCode) {
                        badRequestLinksList.add(url);
                    }
                }
            }
        }catch (IllegalArgumentException e) {
            System.out.println(e);
        }
        return badRequestLinksList;
    }

    @Test
    public void ModusTaskMovingRequests() throws IOException, InterruptedException {
        ArrayList<Integer> list2 = new ArrayList<Integer>();
        File file = new File(
                "D:\\CBBProject\\src\\main\\java\\api\\my_list");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        while ((st = br.readLine()) != null)
            list2.add(Integer.parseInt(st.substring(0, st.indexOf(' '))));
        for (int i : list2
        ) {
//            int respCode;
//            URL url = new URL(String.format("https://iautom8-demo-iautom8-demo-test.azurewebsites.net/api/tasks/%d/unstuck", i));
//            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
//            httpCon.setRequestProperty("Authorization","Bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhZG1pbkBnb21vZHVzOC5jb20iLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6ImIxZjA2YjM4LWMxZGYtNDc0ZS1hNDI2LTQyYjJhMDRjNzIwNiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkFkbWluIiwiZnVsbE5hbWUiOiJBZG1pbiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcHJpbWFyeXNpZCI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL3NpZCI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsInN1YiI6ImFkbWluQGdvbW9kdXM4LmNvbSIsImp0aSI6ImY0NDE4MmNmLTQ1OGEtNDFjMC1hNjhhLTNmN2ZiN2ZlNjI0NCIsImlhdCI6MTYzNDIxOTU0MSwibmJmIjoxNjM0MjE5NTQxLCJleHAiOjE2MzQ1Nzk1NDEsImlzcyI6Imh0dHBzOi8vYXBwLklBdXRvTTguY29tIiwiYXVkIjoiSUF1dG9NOC1hcHAifQ.wSRgQi5PBqqLmZLYr6FuDdgiQP-oIv-isGbjWd_XEmQ");
//            httpCon.setDoOutput(true);
//            httpCon.setRequestMethod("PUT");
//            OutputStreamWriter out = new OutputStreamWriter(
//                    httpCon.getOutputStream());
//            out.write("Resource content");
//            out.close();
//            httpCon.getInputStream();
            URL url = new URL(String.format("https://iautom8-demo-iautom8-demo-test.azurewebsites.net/api/tasks/%d/unstuck", i));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestProperty("Authorization","Bearer "+" eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhZG1pbkBnb21vZHVzOC5jb20iLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6ImIxZjA2YjM4LWMxZGYtNDc0ZS1hNDI2LTQyYjJhMDRjNzIwNiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkFkbWluIiwiZnVsbE5hbWUiOiJBZG1pbiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcHJpbWFyeXNpZCI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL3NpZCI6IjJhM2I5OWU2LTBhOWYtNDJkZC0xNWI1LTA4ZDdhNjY1MDRiNyIsInN1YiI6ImFkbWluQGdvbW9kdXM4LmNvbSIsImp0aSI6ImY0NDE4MmNmLTQ1OGEtNDFjMC1hNjhhLTNmN2ZiN2ZlNjI0NCIsImlhdCI6MTYzNDIxOTU0MSwibmJmIjoxNjM0MjE5NTQxLCJleHAiOjE2MzQ1Nzk1NDEsImlzcyI6Imh0dHBzOi8vYXBwLklBdXRvTTguY29tIiwiYXVkIjoiSUF1dG9NOC1hcHAifQ.wSRgQi5PBqqLmZLYr6FuDdgiQP-oIv-isGbjWd_XEmQ");
            //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("PUT");
//            HttpURLConnection huc;
//            String url = String.format("https://iautom8-demo-iautom8-demo-test.azurewebsites.net/api/tasks/%d/unstuck", i);
//            huc = (HttpURLConnection) (new URL(null, url, new sun.net.www.protocol.https.Handler()).openConnection());
//            huc.setRequestMethod("PUT");
//            huc.connect();
//            respCode = huc.getResponseCode();
//            System.out.println(respCode);
            System.out.println(url);
        }
    }
}
