package pageObjects;

import enums.Credentials;
import org.openqa.selenium.By;

public class StartPage extends AbstractPage {

    private final static By loginButton = By.linkText("Log in");
    private final static By emailField = By.cssSelector("#dialog-signin-tbUserName");
    private final static By passwordField = By.cssSelector("#dialog-signin-tbPassword");
    private final static By acceptCredentialsButton = By.id("dialog-signin-submit");
    private final static By userName = By.xpath("//a[contains(text(),'Stepan')]");
    private final static By invalidLoginMsg = By.xpath("//div[@id='dialog-signin-spanLogonMessage']");


    public boolean validLogination() {
        getElement(loginButton).click();
        getElement(emailField).sendKeys(Credentials.VALID_EMAIL.getValue());
        getElement(passwordField).sendKeys(Credentials.VALID_PASSWORD.getValue());
        getElement(acceptCredentialsButton).click();
        return getElement(userName).isDisplayed();
    }
    public boolean invalidEmail() {
        getElement(loginButton).click();
        getElement(emailField).sendKeys(Credentials.INVALID_EMAIL.getValue());
        getElement(passwordField).sendKeys(Credentials.VALID_PASSWORD.getValue());
        getElement(acceptCredentialsButton).click();
        return getElement(invalidLoginMsg).isDisplayed();
    }
    public boolean invalidPassword() {
        getElement(loginButton).click();
        getElement(emailField).sendKeys(Credentials.VALID_EMAIL.getValue());
        getElement(passwordField).sendKeys(Credentials.INVALID_PASSWORD.getValue());
        getElement(acceptCredentialsButton).click();
        return getElement(invalidLoginMsg).isDisplayed();
    }
    public boolean invalidEmailInvalidPassword() {
        getElement(loginButton).click();
        getElement(emailField).sendKeys(Credentials.INVALID_EMAIL.getValue());
        getElement(passwordField).sendKeys(Credentials.INVALID_PASSWORD.getValue());
        getElement(acceptCredentialsButton).click();
        return getElement(invalidLoginMsg).isDisplayed();
    }
    public boolean emptyEmailAndLoginFields() {
        getElement(loginButton).click();
        getElement(acceptCredentialsButton).click();
        return getElement(invalidLoginMsg).isDisplayed();
    }
}
