package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class Footer extends AbstractPage{
    private static final By footerElementsSelector = By.cssSelector(".list-inline a");

    public ArrayList<WebElement> getFooterLinks() {
        List<WebElement> footerElementList =  getElements(footerElementsSelector);
        return new ArrayList<>(footerElementList);
    }
}
