package pageObjects;

import drivers.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class BuyPage extends AbstractPage{
    // Page Navigate locators
    public static final String buyPageTitle = "Miami FL Condos for sale | CondoBlackBook";
    public static final By buyPage = By.linkText("Buy");
    public static final By buyPageCondoResultSelector = By.className("js-unit");
    // Unit locators
    public static final By unitStatusLocator = By.xpath("//div[contains(text(),'sold')]");
    public static final By unitPriceEstimateButtonLocator = By.xpath("//a[contains(text(),'Price estimate')]");
    // Filter locators
    public static final By openFilterButton = By.xpath("//body/nav[@id='pageHeader']/div[@id='filterContainer']/div[1]/div[3]/ul[1]/li[3]/a[1]");
    public static final By forSaleCheckBox = By.xpath("//body/nav[@id='pageHeader']/div[@id='filterContainer']/div[1]/div[3]/ul[1]/li[3]/div[1]/form[1]/ul[1]/li[1]/div[1]/label[1]");
    public static final By soldCheckBox = By.xpath("//body/nav[@id='pageHeader']/div[@id='filterContainer']/div[1]/div[3]/ul[1]/li[3]/div[1]/form[1]/ul[1]/li[3]/div[1]/label[1]");

    public boolean openBuyPage() {
        getElement(buyPage).click();
        return DriverFactory.getDriver().getTitle().equals(buyPageTitle);
    }
    public void setFiltersToSold() throws InterruptedException {
        getElement(openFilterButton).click();
        getElement(soldCheckBox).click();
        System.out.println(getElement(soldCheckBox).isSelected());
        System.out.println(getElement(forSaleCheckBox).isSelected());
        getElement(forSaleCheckBox).click();
        Thread.sleep(3500);
    }

    public List<WebElement> getBuyPageCondosResult() {
        return getElements(buyPageCondoResultSelector);
    }
    public boolean getUnitStatus() {
        return getElement(unitStatusLocator).isDisplayed();
    }
    public boolean getPriceEstimateButton() {
        return getElement(unitPriceEstimateButtonLocator).isDisplayed();
    }
}
