package pageObjects;

import drivers.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import java.util.List;

public class AbstractPage {

    private static FluentWait<WebDriver> newWait() {
        return new FluentWait<>(DriverFactory.getDriver())
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
    }

    public static WebElement getElement(By locator) {

        return newWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static List<WebElement> getElements(By locator) {
        return DriverFactory.getDriver().findElements(locator);
    }

    public static void scrollToElement(By locator) {
        ((JavascriptExecutor)DriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                DriverFactory.getDriver().findElement(locator));
    }


}
