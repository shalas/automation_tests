package enums;

public enum Urls {

    PRODUCTION_URL("https://www.condoblackbook.com"),
    STAGE_URL("https://www.condoblackbook.net/");

    String value;
    Urls(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
