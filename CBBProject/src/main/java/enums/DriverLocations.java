package enums;

public enum DriverLocations {

    CHROME_DRIVER_LOCATION("src\\main\\java\\drivers\\chromedriver.exe"),
    FIREFOX_DRIVER_LOCATION("src/main/resources/firefoxdriver.exe");

    public String getValue() {
        return value;
    }

    String value;
    DriverLocations(String value){

        this.value = value;
    }
}
