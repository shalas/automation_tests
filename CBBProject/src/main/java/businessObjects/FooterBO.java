package businessObjects;

import org.openqa.selenium.WebElement;
import pageObjects.Footer;

import java.util.List;

public class FooterBO {
    private final Footer footer = new Footer();

    public void goToFooterLink(String pageName) {
        List <WebElement> footersLinksList = footer.getFooterLinks();
        for (WebElement webElement : footersLinksList) {
            if (webElement.getText().toLowerCase().contains(pageName.toLowerCase()))
                webElement.click();
        }
    }
    public void one(){
        List <WebElement> footersLinksList = footer.getFooterLinks();
        for (WebElement webElement : footersLinksList) {

        }

    }
}
