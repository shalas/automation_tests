package base.test.page;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import drivers.DriverFactory;
import enums.Urls;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

public class BaseTestPage extends DriverFactory{
    public static ExtentReports extent = new ExtentReports();
    public static ExtentSparkReporter spark = new ExtentSparkReporter("reports/CBB_Automation_tests.html");

    @BeforeMethod
    public void beforeMethod() {
        DriverFactory.getDriver().get(Urls.PRODUCTION_URL.getValue());
        extent.attachReporter(spark);
    }

    @AfterMethod
    public  void afterMethod(){
        DriverFactory.quit();
        extent.flush();
    }
}
