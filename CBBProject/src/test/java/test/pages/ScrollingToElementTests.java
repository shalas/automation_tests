package test.pages;

import base.test.page.BaseTestPage;
import drivers.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import pageObjects.BuyPage;

public class ScrollingToElementTests extends BaseTestPage {
    BuyPage buyPage = new BuyPage();

    private final By priceEstimateButton = By.xpath("//h2[contains(text(),'Condo Value Estimate')]");
    private final By condoValueBlock = By.xpath("//h2[contains(text(),'Condo Value Estimate')]");

    @Test
    public void ScrollToElementTest() throws InterruptedException {
        buyPage.openBuyPage();
        buyPage.setFiltersToSold();
        Thread.sleep(1000);
        getDriver().findElement(By.xpath("//body/div[@id='search-page-container']/div[@id='pageContainer']/div[2]/div[1]/div[2]/div[2]/div[2]/article[1]/a[1]")).click();
        Thread.sleep(2000);
        DriverFactory.getDriver().findElement(priceEstimateButton).click();
        Thread.sleep(5000);
        JavascriptExecutor jsGetFirstPosition = (JavascriptExecutor) getDriver();
        Long startScrollPosition = (Long) jsGetFirstPosition.executeScript("return window.pageYOffset;");
        System.out.println(startScrollPosition);
//        ((JavascriptExecutor)getDriver()).executeScript("arguments[0].scrollIntoView(true);",getDriver().findElement(condoValueBlock));
        Actions actions = new Actions(getDriver());
        actions.moveToElement(getDriver().findElement(By.xpath("//h2[contains(text(),'Condo Value Estimate')]")));
        actions.perform();
        Thread.sleep(5000);
        JavascriptExecutor jsGetLastPosition = (JavascriptExecutor) getDriver();
        Long endScrollPosition = (Long) jsGetLastPosition.executeScript("return window.pageYOffset;");
        System.out.println(endScrollPosition);
    }
}
