package test.pages;

import api.ApiPage;
import base.test.page.BaseTestPage;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.TimeoutException;
import org.testng.annotations.Test;
import pageObjects.BuyPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class BuyPageTest extends BaseTestPage {
    BuyPage buyPage = new BuyPage();
    ApiPage apiPage = new ApiPage();

    @Test(priority = 1)
    void checkingLinksOnTheBuyPage() throws IOException, InterruptedException {
        buyPage.openBuyPage();
        Thread.sleep(2500);
        ExtentTest extentTest = extent.createTest("Buy page links testing. Verifying all links on responding to Success status code");
        List<String> apiResult = apiPage.checkLinksOnThePage();
        if (apiResult.size() > 0)
            extentTest.fail(apiResult.toString());
        else
            extentTest.pass("All links are clickable and respond with Success status code");
    }

    @Test(priority = 2)
    public void soldUnitsTest() throws InterruptedException {
        ExtentTest extentTest = extent.createTest("Sold Units testing");
        int unitsPerPage = 1;
        String unitName = "";
        for (int i=0; i < unitsPerPage; i++
        ) {
            ExtentTest node = extentTest.createNode("Checking Sold Unit webElements");
                try {
                    buyPage.openBuyPage();
                    buyPage.setFiltersToSold();
                    unitName = buyPage.getBuyPageCondosResult().get(i).getText();
                    buyPage.getBuyPageCondosResult().get(i).click();
                    if (buyPage.getUnitStatus() && buyPage.getPriceEstimateButton())
                        node.pass(unitName);
                }catch (TimeoutException e ) {
                    node.fail(unitName);
                    node.fail(e.getMessage());
                }
            }
    }


}
