package test.pages;

import api.ApiPage;
import base.test.page.BaseTestPage;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.testng.annotations.Test;
import pageObjects.StartPage;

import java.io.IOException;
import java.util.List;

public class StartPageTests extends BaseTestPage {

    StartPage startPage = new StartPage();
    ApiPage apiPage = new ApiPage();


    @Test(priority = 1)
    void validLogination() {
        ExtentTest extentTest = extent.createTest("Logination test. Valid logination");
        extentTest.log(Status.PASS,"Entering valid email");
        extentTest.log(Status.PASS,"Entering invalid password");
        assert startPage.validLogination();
        extentTest.pass("User successfully log in");
    }

    @Test(priority = 2)
    void loginationInvalidPassword() {
        ExtentTest extentTest = extent.createTest("Logination test. Invalid password");
        extentTest.log(Status.PASS,"Entering valid email");
        extentTest.log(Status.PASS,"Entering invalid password");
        assert startPage.invalidPassword();
        extentTest.pass("Error message is displayed. User are not log in");
    }

    @Test(priority = 3)
    void loginationInvalidEmail() {
        ExtentTest extentTest = extent.createTest("Logination test. Invalid email");
        extentTest.log(Status.PASS,"Entering invalid email");
        extentTest.log(Status.PASS,"Entering valid password");
        assert startPage.invalidEmail();
        extentTest.pass("Error message is displayed. User are not log in");
    }

    @Test(priority = 4)
    void loginationInvalidEmailAndPassword() {
        ExtentTest extentTest = extent.createTest("Logination test. Invalid email and password");
        extentTest.log(Status.PASS,"Entering invalid email");
        extentTest.log(Status.PASS,"Entering invalid password");
        assert startPage.invalidEmailInvalidPassword();
        extentTest.pass("Error message is displayed. User are not log in");
    }

    @Test(priority = 5)
    void emptyCredentialsFields() {
        ExtentTest extentTest = extent.createTest("Logination test. Empty email and password field");
        extentTest.log(Status.PASS, "Email field is empty");
        extentTest.log(Status.PASS, "Password field is empty");
        extentTest.log(Status.PASS, "Click on  \"LOG IN\" button");
        assert startPage.emptyEmailAndLoginFields();
        extentTest.pass("Error message is displayed. User are not log in");
    }

    @Test(priority = 6)
    void checkingLinksOnTheStartPage() throws IOException {
        ExtentTest extentTest = extent.createTest("Start page links testing. Verifying all links on responding to Success status code");
        List<String> apiResult = apiPage.checkLinksOnThePage();
        if (apiResult.size() > 0)
            extentTest.fail(apiResult.toString());
        else
            extentTest.pass("All links are clickable and respond with Success status code");
    }
}
